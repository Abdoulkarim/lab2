package lab2.abdoulkarim.cosc431.towson.edu.lab2_dambo;

import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    RadioGroup rg;
    double tem_to_convert=0;
    double result=0;
    TextView input;
    TextView output;
    Button convert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        input= (TextView) findViewById(R.id.input);
        output= (TextView) findViewById(R.id.output);
        rg= (RadioGroup)findViewById(R.id.rg);
        convert=(Button)findViewById(R.id.convert);

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId==R.id.F){
                    tem_to_convert= Double.parseDouble(input.getText().toString());
                    result= tem_to_convert*1.8+32;
                }else if(checkedId==R.id.C){
                    tem_to_convert= Double.parseDouble(input.getText().toString());
                    result= (tem_to_convert-32)/1.8;
                }

            }
        });

        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                output.setText(Double.toString(result));

            }
        });
    }
}
